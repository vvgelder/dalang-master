Dalang master
==============

description
-----------

Setup puppet master with puppetDB

dalang
------
Everything needs a name, so I gave this project the codename Dalang. Dalang is the javanese word for puppet master.
This projects aims to provide a complete and quick setup for a LAMP development enviroment using vagrant and puppet.
The dalang master is te first part of this project.


vagrant
-------

git
---

git clone:

   git clone https://vvgelder@bitbucket.org/vvgelder/dalang-master.git

Download submodules
   git submodule init
   git submodule update

Apply puppet 

   puppet apply -v  --modulepath=puppet/modules/  puppet/manifests/default.pp   
