#!/bin/bash

# check if script is running as root
if [ "$EUID" -ne "0" ]; then
  echo "This script must be run as root." >&2
  exit 1
fi

# update 
apt-get -q update  
#apt-get -qy upgrade
apt-get install -y wget >/dev/null

. /etc/lsb-release

# install puppetlabs apt repo for the appropiate distro
tmpdeb=$(mktemp --suffix=.deb)
wget -O $tmpdeb "http://apt.puppetlabs.com/puppetlabs-release-${DISTRIB_CODENAME}.deb"

dpkg -i $tmpdeb

apt-get -q update

# install puppet agent
apt-get install -y puppet

# enable puppet agent
sed -i 's/START=no/START=yes/' /etc/default/puppet
