class master {

    $confdir = '/etc/puppet'
    $configs = '/vagrant/puppet/modules/master/files'
    
	package { 'puppetmaster-passenger':
        ensure   => 'installed',
    } -> exec { "run master once for certs":
	    command      => "puppet cert generate $fqdn",
		creates      => "/var/lib/puppet/ssl/private_keys/${fqdn}.pem",
		cwd          => "$confdir",
		environment  => "HOME=/root",
        path         => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
	}
	
    package { [ 'git', 'ruby1.8-dev', 'rubygems' ]:
        ensure   => 'installed',
    } 
	
	package { [ 'puppet-module', 'hiera', 'hiera-puppet', 'puppet-lint']:
        ensure   => 'installed',
        provider => 'gem',
	}

	package { 'rack':
	    ensure   => '1.0.1',
		provider => 'gem',
    }
	
	package { 'librarian-puppet':
        ensure => 'installed',
        provider => 'gem',
    } -> file { "$confdir/Puppetfile":
        ensure  => 'present',
        source  => "$configs/Puppetfile",
        owner   => 'root',
        group   => 'root',
    } -> file { "$confdir/modules":
        ensure  => absent,
        recurse => true,
        backup  => false,
    } -> exec { "install modules":
        command      => "librarian-puppet install",
		cwd          => "$confdir",
		environment  => "HOME=/root",
        path         => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    }
	
    
    file { "$confdir/auth.conf":
        ensure => 'present',
        source => "$configs/auth.conf",
        owner  => 'root',
        group  => 'root',
    }

    file { "$confdir/fileserver.conf": 
        ensure => 'present',
        source => "$configs/fileserver.conf",
        owner  => 'root',
        group  => 'root',
    }

    file { "$confdir/hiera.yaml":
        ensure => 'present',
        source => "$configs/hiera.yaml",
        owner  => 'root',
        group  => 'root',
    }

    file { "$confdir/puppet.conf":
        ensure => 'present',
        source => "$configs/puppet.conf",
        owner  => 'root',
        group  => 'root',
    }

    # environment directory
    file { 'environment':
        path    => "$confdir/environment",
        ensure  => 'directory',
        owner   => 'root',
        group   => 'root',
    } -> file { 'environment hieradata':
        path    => "$confdir/environment/hieradata",
        ensure  => 'directory',
    } -> file { 'environment hieradata nodes':
        path    => "$confdir/environment/hieradata/nodes",
        ensure  => 'directory',
    } -> file { 'environment modules':
        path    => "$confdir/environment/modules",
		ensure  => 'directory',
    } -> vcsrepo { "$confdir/environment/modules/role":
        ensure   => present,
        provider => git,
        source   => "https://vvgelder@bitbucket.org/vvgelder/dalang-roles.git",
    } -> vcsrepo { "$confdir/environment/modules/profile":
        ensure   => present,
        provider => git,
        source   => "https://vvgelder@bitbucket.org/vvgelder/dalang-profiles.git",
    }
	
	file { 'global files dir':
        path  => "/etc/puppet/files",
        ensure => 'directory',
    }
}
